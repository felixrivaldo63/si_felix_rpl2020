<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class NamaProduk extends CI_Model{
    public function getNamaProduk()
    {
        
        $arr_nama[] = array('B001','Asbes',10,70000);
        $arr_nama[] = array('B002','Kayu Borneo',10,20000);
        $arr_nama[] = array('B003','Kayu Balok Meranti', 7, 180000);
        $arr_nama[] = array('B004','Pipa PVC 4inc', 5, 150000);
        $arr_nama[] = array('B005','Besi Beton', 3, 160000);
        $arr_nama[] = array('B006','Triplex', 8, 70000);
        $arr_nama[] = array('B007','Cat Aquaproof', 10, 200000);
        $arr_nama[] = array('B008','Semen Tiga Roda', 20, 65000);
        $arr_nama[] = array('B009','Batako', 5, 2000);
        $arr_nama[] = array('B010','Batu Bata Jumbo', 10, 1500);

        return $arr_nama;
    }
}
?>