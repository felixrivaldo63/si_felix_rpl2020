-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 23, 2020 at 05:59 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `rpl_sikp`
--

-- --------------------------------------------------------

--
-- Table structure for table `dosen`
--

CREATE TABLE `dosen` (
  `nik` char(7) NOT NULL,
  `namaDosen` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(10) NOT NULL,
  `koor` int(2) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `dosen`
--

INSERT INTO `dosen` (`nik`, `namaDosen`, `email`, `password`, `koor`) VALUES
('074E324', 'Erick Kurniawan, S.Kom.,M.Kom.', 'erick@staff.ukdw.ac.id', 'rahasia', 0),
('104E344', 'Jong Jek Siang, Drs, M.Sc', 'jjsiang@staff.ukdw.ac.id', 'rahasia', 0),
('174E433', 'Argo Wibowo, S.T., M.T.', 'argo@staff.ukdw.ac.id', 'rahasia', 0),
('944E219', 'Yetli Oslan, S.Kom, M.T.', 'yetli@staff.ukdw.ac.id', 'rahasia', 0),
('944E220', 'Katon Wijana, S.Kom, M.T.', 'katon@staff.ukdw.ac.id', 'rahasia', 0);

-- --------------------------------------------------------

--
-- Table structure for table `kp`
--

CREATE TABLE `kp` (
  `idKp` int(2) NOT NULL,
  `idReg` int(2) DEFAULT NULL,
  `judul` varchar(200) DEFAULT NULL,
  `tools` varchar(200) DEFAULT NULL,
  `spesifikasi` varchar(200) DEFAULT NULL,
  `lembaga` varchar(30) DEFAULT NULL,
  `pimpinan` varchar(30) DEFAULT NULL,
  `noTelp` varchar(15) DEFAULT NULL,
  `alamat` varchar(200) DEFAULT NULL,
  `fax` varchar(15) DEFAULT NULL,
  `dokKp` varchar(12) DEFAULT NULL,
  `statusUjiKp` char(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `kp`
--

INSERT INTO `kp` (`idKp`, `idReg`, `judul`, `tools`, `spesifikasi`, `lembaga`, `pimpinan`, `noTelp`, `alamat`, `fax`, `dokKp`, `statusUjiKp`) VALUES
(2, 3, 'Pembuatan Sistem Informasi Penilaian Karyawan', 'Visual Studio Code', 'Teknologi penilai karyawan', 'Bank Central Asia Tbk', 'Anthony Salim', '(021)23588000', 'Menara BCA', '(021)23588000', '72180003.pdf', '0'),
(3, 4, 'Pengembangan Pengelolaan Dashboard', 'Visual Studio Code', 'Pengelolaan dashboard', 'Sinar Mas', 'Eka Tjipta Widjaja', '(021)50188888', 'Sinar Mas Land Plaza Tower I', '(021)50188888', '72180004.pdf', '0');

-- --------------------------------------------------------

--
-- Table structure for table `mahasiswa`
--

CREATE TABLE `mahasiswa` (
  `nim` char(8) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `mahasiswa`
--

INSERT INTO `mahasiswa` (`nim`, `nama`, `email`, `password`) VALUES
('72180001', 'Felix Rivaldo', 'felix.rivaldo@si.ukdw.ac.id', 'rahasia'),
('72180002', 'Magdalena Evelyn', 'magdalena.evelyn@si.ukdw.ac.id', 'rahasia'),
('72180003', 'Nadia Angelica', 'nadia.angelica@si.ukdw.ac.id', 'rahasia'),
('72180004', 'Elbie Reyova', 'elbie.reyova@si.ukdw.ac.id', 'rahasia'),
('72180005', 'Rico Alex', 'rico.alex@si.ukdw.ac.id', 'rahasia');

-- --------------------------------------------------------

--
-- Table structure for table `periode`
--

CREATE TABLE `periode` (
  `idPeriode` int(2) NOT NULL,
  `semester` varchar(5) DEFAULT NULL,
  `tahun` varchar(9) DEFAULT NULL,
  `batasKp` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `periode`
--

INSERT INTO `periode` (`idPeriode`, `semester`, `tahun`, `batasKp`) VALUES
(1, 'Gasal', '2019', NULL),
(2, 'Genap', '2020', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `permintaanujian`
--

CREATE TABLE `permintaanujian` (
  `idPerUjian` int(2) NOT NULL,
  `nim` char(8) DEFAULT NULL,
  `idKp` int(2) DEFAULT NULL,
  `tglUjian` date DEFAULT NULL,
  `jamUjian` time DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `permintaanujian`
--

INSERT INTO `permintaanujian` (`idPerUjian`, `nim`, `idKp`, `tglUjian`, `jamUjian`) VALUES
(1, '72180003', 2, '2020-05-21', '09:00:00'),
(2, '72180004', 3, '2020-06-15', '10:30:00');

-- --------------------------------------------------------

--
-- Table structure for table `prakp`
--

CREATE TABLE `prakp` (
  `idPraKp` int(2) NOT NULL,
  `idReg` int(2) DEFAULT NULL,
  `judul` varchar(200) DEFAULT NULL,
  `tools` varchar(200) DEFAULT NULL,
  `spesifikasi` varchar(200) DEFAULT NULL,
  `lembaga` varchar(30) DEFAULT NULL,
  `pimpinan` varchar(30) DEFAULT NULL,
  `noTelp` varchar(15) DEFAULT NULL,
  `alamat` varchar(200) DEFAULT NULL,
  `fax` varchar(15) DEFAULT NULL,
  `dokPraKp` varchar(12) DEFAULT NULL,
  `statusPraKp` char(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `prakp`
--

INSERT INTO `prakp` (`idPraKp`, `idReg`, `judul`, `tools`, `spesifikasi`, `lembaga`, `pimpinan`, `noTelp`, `alamat`, `fax`, `dokPraKp`, `statusPraKp`) VALUES
(1, 1, 'Pembuatan Sistem Informasi Jual Beli', 'Visual Studio Code', 'Ruang lingkup teknologi, untuk user', 'Tokopedia', 'William Tanuwijaya', '(021)50170809', 'Tokopedia Tower', '(021)50170809', '72180001.pdf', '0'),
(2, 2, 'Rancang Aplikasi Pesan Tiket Online', 'Visual Studio Code', 'Teknologi memudahkan user', 'Blibli', 'Kusumo Martanto', '(021)25556060', 'Jalan Aipda K.S. Tubun II C No. 8. Jakarta Barat', '(021)25556060', '72180002.pdf', '0');

-- --------------------------------------------------------

--
-- Table structure for table `registrasi`
--

CREATE TABLE `registrasi` (
  `idReg` int(2) NOT NULL,
  `idPeriode` int(2) DEFAULT NULL,
  `nim` char(8) DEFAULT NULL,
  `semester` varchar(5) DEFAULT NULL,
  `tahun` varchar(9) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `registrasi`
--

INSERT INTO `registrasi` (`idReg`, `idPeriode`, `nim`, `semester`, `tahun`) VALUES
(1, 1, '72180001', 'Gasal', '2019'),
(2, 1, '72180002', 'Gasal', '2019'),
(3, 1, '72180003', 'Gasal', '2019'),
(4, 1, '72180004', 'Gasal', '2019');

-- --------------------------------------------------------

--
-- Table structure for table `ruang`
--

CREATE TABLE `ruang` (
  `idRuang` int(2) NOT NULL,
  `namaRuang` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `ruang`
--

INSERT INTO `ruang` (`idRuang`, `namaRuang`) VALUES
(1, 'Java'),
(2, 'Kernel'),
(3, 'Hypertext'),
(4, 'Gateway'),
(5, 'Interface');

-- --------------------------------------------------------

--
-- Table structure for table `surat`
--

CREATE TABLE `surat` (
  `idSurat` int(2) NOT NULL,
  `idReg` int(2) DEFAULT NULL,
  `lembaga` varchar(30) DEFAULT NULL,
  `pimpinan` varchar(30) DEFAULT NULL,
  `noTelp` varchar(15) DEFAULT NULL,
  `alamat` varchar(200) DEFAULT NULL,
  `fax` varchar(15) DEFAULT NULL,
  `dokSurat` varchar(12) DEFAULT NULL,
  `statusSurat` char(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `surat`
--

INSERT INTO `surat` (`idSurat`, `idReg`, `lembaga`, `pimpinan`, `noTelp`, `alamat`, `fax`, `dokSurat`, `statusSurat`) VALUES
(1, 1, 'Tokopedia', 'William Tanuwijaya', '(021)50170809', 'Tokopedia Tower', '(021)50170809', '72180001.pdf', '0'),
(2, 2, 'Blibli', 'Kusumo Martanto', '(021)25556060', 'Jalan Aipda K.S. Tubun II C No. 8. Jakarta Barat', '(021)25556060', '72180002.pdf', '0'),
(3, 3, 'Bank Central Asia Tbk', 'Anthony Salim', '(021)23588000', 'Menara BCA', '(021)23588000', '72180003.pdf', '0'),
(4, 4, 'Sinar Mas', 'Eka Tjipta Widjaja', '(021)50188888', 'Sinar Mas Land Plaza Tower I', '(021)50188888', '72180004.pdf', '0');

-- --------------------------------------------------------

--
-- Table structure for table `ujian`
--

CREATE TABLE `ujian` (
  `idUjian` int(2) NOT NULL,
  `idRuang` int(2) DEFAULT NULL,
  `idKp` int(2) DEFAULT NULL,
  `idPerUjian` int(2) DEFAULT NULL,
  `nim` char(8) DEFAULT NULL,
  `nik` char(7) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `ujian`
--

INSERT INTO `ujian` (`idUjian`, `idRuang`, `idKp`, `idPerUjian`, `nim`, `nik`) VALUES
(1, 1, 2, 1, '72180003', '944E220'),
(2, 2, 3, 2, '72180004', '174E433');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `dosen`
--
ALTER TABLE `dosen`
  ADD PRIMARY KEY (`nik`);

--
-- Indexes for table `kp`
--
ALTER TABLE `kp`
  ADD PRIMARY KEY (`idKp`),
  ADD KEY `FK_idRegistrasi_2` (`idReg`);

--
-- Indexes for table `mahasiswa`
--
ALTER TABLE `mahasiswa`
  ADD PRIMARY KEY (`nim`);

--
-- Indexes for table `periode`
--
ALTER TABLE `periode`
  ADD PRIMARY KEY (`idPeriode`);

--
-- Indexes for table `permintaanujian`
--
ALTER TABLE `permintaanujian`
  ADD PRIMARY KEY (`idPerUjian`),
  ADD KEY `FK_nim_2` (`nim`),
  ADD KEY `FK_idKp` (`idKp`);

--
-- Indexes for table `prakp`
--
ALTER TABLE `prakp`
  ADD PRIMARY KEY (`idPraKp`),
  ADD KEY `FK_idRegistrasi` (`idReg`);

--
-- Indexes for table `registrasi`
--
ALTER TABLE `registrasi`
  ADD PRIMARY KEY (`idReg`),
  ADD KEY `FK_idPeriode` (`idPeriode`),
  ADD KEY `FK_nim` (`nim`);

--
-- Indexes for table `ruang`
--
ALTER TABLE `ruang`
  ADD PRIMARY KEY (`idRuang`);

--
-- Indexes for table `surat`
--
ALTER TABLE `surat`
  ADD PRIMARY KEY (`idSurat`),
  ADD KEY `FK_idReg` (`idReg`);

--
-- Indexes for table `ujian`
--
ALTER TABLE `ujian`
  ADD PRIMARY KEY (`idUjian`),
  ADD KEY `FK_idRuang` (`idRuang`),
  ADD KEY `FK_idKp_2` (`idKp`),
  ADD KEY `FK_idPerUjian` (`idPerUjian`),
  ADD KEY `FK_nim_3` (`nim`),
  ADD KEY `FK_nik` (`nik`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `kp`
--
ALTER TABLE `kp`
  MODIFY `idKp` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `periode`
--
ALTER TABLE `periode`
  MODIFY `idPeriode` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `permintaanujian`
--
ALTER TABLE `permintaanujian`
  MODIFY `idPerUjian` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `prakp`
--
ALTER TABLE `prakp`
  MODIFY `idPraKp` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `registrasi`
--
ALTER TABLE `registrasi`
  MODIFY `idReg` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `ruang`
--
ALTER TABLE `ruang`
  MODIFY `idRuang` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `surat`
--
ALTER TABLE `surat`
  MODIFY `idSurat` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `ujian`
--
ALTER TABLE `ujian`
  MODIFY `idUjian` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `kp`
--
ALTER TABLE `kp`
  ADD CONSTRAINT `FK_idRegistrasi_2` FOREIGN KEY (`idReg`) REFERENCES `registrasi` (`idReg`);

--
-- Constraints for table `permintaanujian`
--
ALTER TABLE `permintaanujian`
  ADD CONSTRAINT `FK_idKp` FOREIGN KEY (`idKp`) REFERENCES `kp` (`idKp`),
  ADD CONSTRAINT `FK_nim_2` FOREIGN KEY (`nim`) REFERENCES `mahasiswa` (`nim`);

--
-- Constraints for table `prakp`
--
ALTER TABLE `prakp`
  ADD CONSTRAINT `FK_idRegistrasi` FOREIGN KEY (`idReg`) REFERENCES `registrasi` (`idReg`);

--
-- Constraints for table `registrasi`
--
ALTER TABLE `registrasi`
  ADD CONSTRAINT `FK_idPeriode` FOREIGN KEY (`idPeriode`) REFERENCES `periode` (`idPeriode`),
  ADD CONSTRAINT `FK_nim` FOREIGN KEY (`nim`) REFERENCES `mahasiswa` (`nim`);

--
-- Constraints for table `surat`
--
ALTER TABLE `surat`
  ADD CONSTRAINT `FK_idReg` FOREIGN KEY (`idReg`) REFERENCES `registrasi` (`idReg`);

--
-- Constraints for table `ujian`
--
ALTER TABLE `ujian`
  ADD CONSTRAINT `FK_idKp_2` FOREIGN KEY (`idKp`) REFERENCES `kp` (`idKp`),
  ADD CONSTRAINT `FK_idPerUjian` FOREIGN KEY (`idPerUjian`) REFERENCES `permintaanujian` (`idPerUjian`),
  ADD CONSTRAINT `FK_idRuang` FOREIGN KEY (`idRuang`) REFERENCES `ruang` (`idRuang`),
  ADD CONSTRAINT `FK_nik` FOREIGN KEY (`nik`) REFERENCES `dosen` (`nik`),
  ADD CONSTRAINT `FK_nim_3` FOREIGN KEY (`nim`) REFERENCES `mahasiswa` (`nim`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
